var comentarios_cuenta = document.getElementById("comentario");
var nombre_cuenta = document.getElementById("nombre");
var cuenta = document.getElementById("largo");
var letras_nombre = 0;
var letras_comentario= 0;

nombre_cuenta.addEventListener("keyup", function () {
    letras_nombre = nombre_cuenta.value.split('');
    if (letras_nombre > 20) {
        nombre_cuenta.value = nombre_cuenta.value.substring(0, 20);
    }
})

comentarios_cuenta.addEventListener("keyup", function () {
    letras_comentario = comentarios_cuenta.value.split('');
    cuenta.innerText = letras_comentario.length;
    if (letras_comentario.length > 200) {
        comentarios_cuenta.value = comentarios_cuenta.value.substring(0, 200);
    }
})

function post_coment() {
    if (letras_nombre.length >= 3) {
        if (letras_comentario.length > 0) {
            control_master();
            letras_nombre = "";
            letras_comentario = "";
        } else {
            alert("comentario no debe estar vacio")
        }
    } else {
        alert("nombre inferior a 3 caracteres");
    }
}

function control_master() {
    data_recovery();
    form_cleaner();
    coment_remove();
    cuenta.innerText = 0;
}

function data_recovery() {
    var nombre = document.getElementById('nombre').value;
    var comentario = document.getElementById('comentario').value;
    var date = new Date().toISOString().slice(0,10);

    gestor_comentarios(nombre, comentario, date);
}

function form_cleaner() {
    var nombre = document.getElementById('nombre').value = '';
    var comentario = document.getElementById('comentario').value = '';
}

function gestor_comentarios(nombre, comentario, date) {
    var comentario_div = document.getElementById('caja_comentarios');

    var new_card = document.createElement('div');
    new_card.className += "card col-md-3";
    new_card.style.property = "width: 18rem;"

    var new_card_body = document.createElement('div');
    new_card_body.className += "card-body";

    var new_card_title = document.createElement('h5')
    new_card_title.className += "card-title";

    var new_card_title_text = document.createTextNode(nombre);
    new_card_title.appendChild(new_card_title_text);

    var new_card_sub_title = document.createElement('h6')
    new_card_sub_title.className += "card-subtitle mb-2 text-muted";
    var new_card_sub_title_text = document.createTextNode(date);
    new_card_sub_title.appendChild(new_card_sub_title_text);

    var new_card_coment = document.createElement('p');
    new_card_coment.className += "card-text";
    var new_card_coment_text = document.createTextNode(comentario);
    new_card_coment.appendChild(new_card_coment_text);



    new_card.appendChild(new_card_body);
    new_card_body.appendChild(new_card_title);
    new_card_body.appendChild(new_card_sub_title);
    new_card_body.appendChild(new_card_coment);

    comentario_div.appendChild(new_card);
}

function coment_remove() {
    var lista_comentarios = document.getElementById('caja_comentarios');
    var lista_comentarios_largo = lista_comentarios.getElementsByClassName('card').length;
    console.log(lista_comentarios_largo);
    if (lista_comentarios_largo >= 5) {
        lista_comentarios.getElementsByTagName("div")[0].remove();
    }

}